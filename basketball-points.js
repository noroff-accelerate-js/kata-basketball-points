function points(two_pointers, three_pointers) {
    return two_pointers * 2 + three_pointers * 3;
}

console.log(points(1, 1));
console.log(points(7, 5));
console.log(points(38, 8));
console.log(points(0, 1));
console.log(points(0, 0));